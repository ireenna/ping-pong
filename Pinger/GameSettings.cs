﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pinger
{
    static class GameSettings
    {
        public static int Max_score { get; set; } = 5;
        public static Level Lvl { get; set; } = Level.Easy;
    }
}
